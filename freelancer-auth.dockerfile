FROM microsoft/dotnet:1.1.1-sdk

COPY ./src/Freelancer.Auth/Freelancer.Auth.csproj /app/
COPY ./NuGet.Config /app/
WORKDIR /app/
RUN dotnet restore
#migrate database
ADD ./src/Freelancer.Auth/ /app/
ARG PUBLISH_MODE=Debug
RUN dotnet publish -c ${PUBLISH_MODE} -o out
#RUN sleep 5
EXPOSE 80
ENTRYPOINT ["dotnet", "out/Freelancer.Auth.dll"]