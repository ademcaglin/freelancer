FROM microsoft/dotnet:1.1.1-sdk

COPY ./src/Freelancer.App/Freelancer.App.csproj /app/
COPY ./NuGet.Config /app/
WORKDIR /app/
RUN dotnet restore
ADD ./src/Freelancer.App/ /app/

ARG PUBLISH_MODE=Debug
RUN echo ${PUBLISH_MODE}
RUN dotnet publish -c ${PUBLISH_MODE} -o out
EXPOSE 80
ENTRYPOINT ["dotnet", "out/Freelancer.App.dll"]