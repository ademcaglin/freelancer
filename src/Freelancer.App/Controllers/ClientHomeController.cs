using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Freelancer.App.Controllers
{
    [Authorize]
    [ShouldBeClient]
    public class ClientHomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
