using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Freelancer.App.Controllers
{
    [Authorize]
    [ShouldBeFreelancer]
    public class FreelancerHomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
