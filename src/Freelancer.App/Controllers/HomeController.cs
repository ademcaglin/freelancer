using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Freelancer.App.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            if (User.FindFirst("UserType").Value == "CLIENT")
            {
                return Redirect("/clienthome");
            }
            if (User.FindFirst("UserType").Value == "FREELANCER")
            {
                return Redirect("/freelancerhome");
            }
            return Content("Error");
        }
        
        public async Task Logout()
        {
            await HttpContext.Authentication.SignOutAsync("Cookies");
            await HttpContext.Authentication.SignOutAsync("oidc");
        }
    }
}
