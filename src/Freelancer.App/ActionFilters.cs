using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Freelancer.App
{
    public class ShouldBeClientAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
            
            if (context.HttpContext.User.FindFirst("UserType").Value != "CLIENT")
            {
                context.Result = new StatusCodeResult(403);
            }
        }
    }

    public class ShouldBeFreelancerAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
             if (context.HttpContext.User.FindFirst("UserType").Value != "FREELANCER")
            {
                context.Result = new StatusCodeResult(403);
            }
        }
    }
}
