using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;

namespace Freelancer.App
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public Startup(ILoggerFactory loggerFactory, IHostingEnvironment environment)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(environment.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment.EnvironmentName}.json", optional: true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
        }
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole().AddDebug();
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            app.UseDeveloperExceptionPage();
            var logger = loggerFactory.CreateLogger<Startup>();
            logger.LogInformation(Configuration["ASPNETCORE_ENVIRONMENT"]);
            logger.LogInformation(Configuration["OPENIDCONNECT_AUTHORITY"]);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationScheme = "Cookies",
                AutomaticAuthenticate = true,
                AutomaticChallenge = true
            });

            app.UseOpenIdConnectAuthentication(new OpenIdConnectOptions
            {
                AuthenticationScheme = "oidc",
                SignInScheme = "Cookies",
                Authority = Configuration["OPENIDCONNECT_AUTHORITY"],
                RequireHttpsMetadata = false,
                ClientId = Configuration["OPENIDCONNECT_CLIENTID"], 
                ResponseType = "id_token",
                TokenValidationParameters = new TokenValidationParameters
                {

                    NameClaimType = "name",
                    RoleClaimType = "role",
                },
                Scope = { "openid", "profile" }
                //Events = new OpenIdConnectEvents
                //{
                //    OnRedirectToIdentityProvider = n =>
                //    {
                //        // acr_values is where you can pass custom hints/params
                //        //n.ProtocolMessage.AcrValues = "tenant:foo";
                //        //n.ProtocolMessage.AcrValues = "idp:Google";
                //        return Task.FromResult(0);
                //    }
                //}

            });

            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }
    }
}
