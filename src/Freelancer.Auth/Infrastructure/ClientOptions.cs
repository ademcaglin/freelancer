using IdentityServer4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Freelancer.Auth.Infrastructure
{
    public class ClientOptions
    {
        public string Client_Home_Url { get; set; }
        
        public string Freelancer_Home_Url { get; set; }
        
        public IList<Client> Clients { get; set; }
    }
}
