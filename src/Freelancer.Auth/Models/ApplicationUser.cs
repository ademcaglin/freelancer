using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Freelancer.Auth.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string ClientName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool Verified { get; set; }
        
        public string UserType { get; set; }
    }
}
