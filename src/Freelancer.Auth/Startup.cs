using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Freelancer.Auth.Models;
using Microsoft.AspNetCore.Identity;
using Freelancer.Auth.Infrastructure;
using Freelancer.Auth.Data;
using IdentityServer4.Services;
using Serilog;
using Microsoft.Extensions.Options;
using System.Threading;
using Microsoft.Extensions.Caching.Distributed;
using System.Text;
using Microsoft.Extensions.Caching.Redis;
using StackExchange.Redis;
using System.Net;
using System.Text.RegularExpressions;

namespace Freelancer.Auth
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public Startup(ILoggerFactory loggerFactory, IHostingEnvironment environment)
        {
            var serilog = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .Enrich.FromLogContext()
                .WriteTo.File(@"bin/Debug/log.txt");
                
            loggerFactory
                .AddSerilog(serilog.CreateLogger());
            
            var builder = new ConfigurationBuilder()
                .SetBasePath(environment.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment.EnvironmentName}.json", optional: true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            /*var connection = string.Format(@"Server=freelancer-auth-db;Database=master;User=sa;Password={0};", Configuration["SA_PASSWORD"]);     
            services.AddDbContext<ApplicationDbContext>(
                  options => options.UseSqlServer(connection));*/ 
            /*services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite("Data Source=freelance-auth.db"));*/  
            var connection = string.Format(@"User ID=postgres;Password={0};Server=freelancer-auth-db;Port=5432;Database=POSTGRES_USER;Integrated Security=true;Pooling=true;",
               Configuration["POSTGRES_PASSWORD"]); 
            services.AddEntityFrameworkNpgsql()
               .AddDbContext<ApplicationDbContext>(
                  options => options.UseNpgsql(connection)); 

            var addresses = Dns.GetHostAddressesAsync(Configuration["REDIS_HOST"]).Result; 
            services.AddDistributedRedisCache(options =>
            {
                options.Configuration = string.Join(",", addresses.Select(x => x.MapToIPv4().ToString() + ":6379"));  
                options.InstanceName = Configuration["REDIS_INSTANCENAME"];
            });
            
   
            services.AddIdentity<ApplicationUser, IdentityRole>()
              .AddEntityFrameworkStores<ApplicationDbContext>()
              .AddDefaultTokenProviders();
            services.AddTransient<IProfileService, IdentityProfileService>();
            services.Configure<ClientOptions>(Configuration.GetSection("ClientOptions"));
            var clientOpts = services.BuildServiceProvider().GetRequiredService<IOptions<ClientOptions>>().Value;
            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                .AddInMemoryClients(clientOpts.Clients)
                .AddAspNetIdentity<ApplicationUser>()
                .AddProfileService<IdentityProfileService>();
               
            services.AddMvc();
            
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                loggerFactory.AddConsole();
                app.UseDeveloperExceptionPage();
                Thread.Sleep(10000);
                using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
                    .CreateScope())
                {
                    serviceScope.ServiceProvider.GetService<ApplicationDbContext>()
                        .Database.Migrate();
                }
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            var cache = app.ApplicationServices.GetRequiredService<IDistributedCache>();
            cache.SetAsync("key", Encoding.UTF8.GetBytes("value"), new DistributedCacheEntryOptions()
            {
                 
            });
            /*var manager = new RedisManagerPool("localhost:6379");
            using (var client = manager.GetClient())
            {
                client.Set("foo", "bar");
                Console.WriteLine("foo={0}", client.Get<string>("foo"));
            }*/
            var logger = loggerFactory.CreateLogger<Startup>();
            logger.LogCritical(Encoding.UTF8.GetString(cache.Get("key")));
            app.UseStaticFiles();

            app.UseIdentity();
            
            app.UseIdentityServer();

            app.UseGoogleAuthentication(new GoogleOptions
            {
                AuthenticationScheme = "Google",
                SignInScheme = "Identity.External", // this is the name of the cookie middleware registered by UseIdentity()
                ClientId = Configuration["GOOGLE_CLIENTID"],
                ClientSecret = Configuration["GOOGLE_CLIENTSECRET"],
            });

            app.UseFacebookAuthentication(new FacebookOptions
            {
                AuthenticationScheme = "Facebook",
                SignInScheme = "Identity.External", // this is the name of the cookie middleware registered by UseIdentity()
                ClientId = "44",
                ClientSecret = "hh",
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
