#!/bin/bash
# wait-for-postgres.sh

set -e

host="$1"
shift
cmd="dotnet ef database update;"

until psql -h "$POSTGRES_HOST" -U "freelancer-auth-db" -c '\l'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - executing command"
#exec $cmd